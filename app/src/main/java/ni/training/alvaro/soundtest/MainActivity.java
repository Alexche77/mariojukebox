package ni.training.alvaro.soundtest;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
    private SoundPool sp;
    String logSonidos = "SONIDOS REPRODUCIDOS: \n";
    Button btnN1, btnHurry, btnAgua;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnN1 = (Button)findViewById(R.id.btnNivel1);
        btnHurry = (Button)findViewById(R.id.btnHurryMario);
        btnAgua = (Button)findViewById(R.id.btnAgua);

        //Notese que la instancia mediaPlayer es global
        //Por practicidad, el primer archivo que se carga es la cancion de super mario
        mediaPlayer = MediaPlayer.create(this,R.raw.supermariobros);
        //Le decimos que lo que queremos es un AudioManager tipo STREAM_MUSIC
//    Antes de crear un sonido para la app debemos especificar que atributos lleva el audio
//        Creamos un AudioAttributes y le especificamos que el contenido es tipo musica
//        Y que es para utilizarse en un juego
//        Referencias: https://developer.android.com/reference/android/media/AudioAttributes.html
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_UNKNOWN)
                .build();

//        Ahora vamos a crear nuestro builder de SoundPool
//        Le decimos que unicamente puede haber 1 sonido a la vez
//        Le especificamos los atributos de los audios a utilizar.
//      Basicamente lo que hacemos es primero verificar si el reproductor es null, entonces
//      creamos uno nuevo, con el archivo que queremos.
        sp = new SoundPool.Builder().setMaxStreams(1).setAudioAttributes(audioAttributes).build();
        btnN1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarMediaPlayer();
                if (mediaPlayer==null){
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.supermariobros);
                    mediaPlayer.start();
                }
            }
        });

        btnAgua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarMediaPlayer();
                if (mediaPlayer==null){
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.waterworld);
                    mediaPlayer.start();
                }
            }
        });

        btnHurry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarMediaPlayer();
                if (mediaPlayer==null){
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.hurrysupermariobros);
                    mediaPlayer.start();
                }
            }
        });
    }

    //Este metodo simplemente cierra el MediaPlayer para volverlo a instanciar en el clic.
    private void limpiarMediaPlayer() {
        if (mediaPlayer!=null)
            mediaPlayer.release();
        mediaPlayer = null;
    }

    public void detenerAudio(View view){
        limpiarMediaPlayer();
    }

    public void saludar(View view){
        Log.d("SONIDO","VIDA");
        reproducir(R.raw.vida);
        agregarLog("Vida");
    }

    public void monedita(View view){
        Log.d("SONIDO","MONEDA");
       reproducir(R.raw.monedita);
        agregarLog("Monedita");

    }

    private void agregarLog(String cadena) {
        logSonidos += "Hora: "+DateFormat.getTimeInstance().format(new Date())+"\tSonido->"+cadena+"\n";
    }

    private void reproducir(int sonido){
        //        Aca utilizamos el builder
//        Recibe 3 params:
//        1-Contexto (this, activity)
//        2-El archivo
//        3-Prioridad 1
        sp.load(this,sonido,1);
        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("SONIDO","El audio se ha cargado->Status:"+status+", Audio ID"+sampleId);
                sp.play(sampleId,1.0f, 1.0f, 1, 0, 1);
            }
        });
    }


    public void mostrarLog(View view){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View content =  inflater.inflate(R.layout.infodialog,null);
        TextView textView = (TextView) content.findViewById(R.id.info);
        textView.setText("----LOG:\n"+logSonidos);
        dialogBuilder.setView(content);

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }


    @Override
    protected void onPause() {
        super.onPause();
        limpiarMediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        limpiarMediaPlayer();
    }
}
